<?php

use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('anggota')->insert([
            'id' => 1,
            'kode' => '131323',
            'nama' => 'Aji',
            'alamat' => 'Bogor',
            'users_id' => 1
        ]);
        
    }
}
