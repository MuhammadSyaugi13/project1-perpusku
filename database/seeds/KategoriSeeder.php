<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori')->insert([
        'id' => 1,
        'nama_kategori' => 'Self Improvement',
        ]);

        DB::table('kategori')->insert([
        'id' => 2,
        'nama_kategori' => 'Fiksi',
        ]);
    }
}
