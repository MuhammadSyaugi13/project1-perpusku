<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku')->insert([
            'id' => 1,
            'judul' => 'Bicara itu ada seninya',
            'penulis' => 'Oh Su Hyang',
            'kategori_id' => 1
        ]);

        DB::table('buku')->insert([
            'id' => 2,
            'judul' => 'Harry Potter',
            'penulis' => 'JK Rowling',
            'kategori_id' => 2
        ]);
        
    }
}
