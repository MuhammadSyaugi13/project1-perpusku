<?php

use Illuminate\Database\Seeder;

class DataPeminjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_peminjaman')->insert([
            'id'=> 1,
            'anggota_id'=> 1,
            'buku_id'=> 1,
            'tgl_peminjaman'=> '2022-03-12 00:00:00',
            'tgl_pengembalian'=> '2022-03-16 00:00:00'
        ]);

        DB::table('anggota')->where('id', 1)->update([
            'data_peminjaman_id' => 1,
        ]);

        DB::table('buku')->where('id', 1)->update([
            'data_peminjaman_id' => 1,
        ]);
    }
}
